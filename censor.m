function strs = censor(str, censorstr)
strs = {};
for i=1:length(str)
    if isempty(strfind(str{i}, censorstr))
        strs{i} = str{i};
    end
end
strs = strs(~cellfun('isempty', strs));
end