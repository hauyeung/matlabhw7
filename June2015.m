function j = June2015()
j = cell(30,3);
for i=1:30
    j{i,1} = 'June';
    j{i,2} = i;
    j{i,3} = datestr(strcat(num2str(i),'-June-2015'),'ddd'); 
end
end