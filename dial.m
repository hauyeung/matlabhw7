function num = dial(letters)
num = '';
for i=1:length(letters)
    switch letters(i)
        case {'A','B','C'}
            num = strcat(num, '2');
        case {'D','E','F'}
            num = strcat(num, '3');
        case {'G','H','I'}
            num = strcat(num, '4');
        case {'J','K','L'}
            num = strcat(num, '5');
        case {'M','N','O'}
            num = strcat(num, '6');
        case {'P','R','S'}
            num = strcat(num, '7');
        case {'T','U','V'}
            num = strcat(num, '8');
        case {'W','X','Y'}
            num = strcat(num, '9');
        case {'(',')','-',' '}
            num = strcat(num,{' '});
        case {'1','2','3','4','5','6','7','8','9','0'}
            num = strcat(num, letters(i));
        otherwise
            num = [];
            break;
    end
end
end
