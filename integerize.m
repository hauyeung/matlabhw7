function class = integerize(A)
m = max(max(A));
if (m > 2^32-1 && m <= 2^64-1)
    class = 'uint64';
elseif (m > 2^16-1 && m <= 2^32-1)
    class = 'uint32';
elseif (m > 2^8-1 && m<= 2^16-1)
    class = 'uint16';
elseif (m >= 0 && m <= 2^8-1)
    class = 'uint8';
else
    class = 'NONE';
end
end