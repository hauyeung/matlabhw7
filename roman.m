function num = roman(str)
switch str
    case {'i','I'}
        num = 1;
    case {'ii','II'}
        num = 2;
    case {'iii','III'}
        num = 3;
    case {'iv','IV'}
        num = 4;
    case {'v','V'}
        num = 5;
    case {'vi','VI'}
        num = 6;
    case {'vii','VII'}
        num = 7;
    case {'viii','VIII'}
        num = 8;
    case {'ix','IX'}
        num = 9;
    case {'X','x'}
        num = 10;
    case {'XI','xi'}
        num = 11;
    case {'xii','XII'}
        num = 12;
    case {'xiii','XIII'}
        num = 13;
    case {'xiv','XIV'}
        num = 14;
    case {'xv','XV'}
        num = 15;
    case {'xvi','XVI'}
        num = 16;
    case {'xvii','XVII'}
        num = 17;
    case {'xviii','XVIII'}
        num = 18;
    case {'xix','XIX'}
        num = 19;
    case {'xx','XX'}
        num = 20;
    otherwise
        num =0;
end
num = uint8(num);
end