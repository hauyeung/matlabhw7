function coded = codeit(txt)
for i=1:length(txt)   
    switch (txt(i))
        case 'a'
            txt(i) = 'z';
        case 'b'
            txt(i) = 'y';
        case 'c'
            txt(i) = 'x';
        case 'd'
            txt(i) = 'w';
        case 'e'
            txt(i) = 'v';
        case 'f'
            txt(i) = 'u';
        case 'g'
            txt(i) = 't';
        case 'h'
            txt(i) = 's';
        case 'i'
            txt(i) = 'r';
        case 'j'
            txt(i) = 'q';
        case 'k'
            txt(i) = 'p';
        case 'l'
            txt(i) = 'o';
        case 'm'
            txt(i) = 'n';
        case 'n'
            txt(i) = 'm';
        case 'o'
            txt(i) = 'l';
        case 'p'
            txt(i) = 'k';
        case 'q'
            txt(i) = 'j';
        case 'r'
            txt(i) = 'i';
        case 's'
            txt(i) = 'h';
        case 't'
            txt(i) = 'g';
        case 'u'
            txt(i) = 'f';
        case 'v'
            txt(i) = 'e';
        case 'w'
            txt(i) = 'd';
        case 'x'
            txt(i) = 'c';
        case 'y'
            txt(i) = 'b';
        case 'z'
            txt(i)   = 'a';            
        case 'A'
            txt(i) = 'Z';
        case 'B'
            txt(i) = 'Y';
        case 'C'
            txt(i) = 'X';
        case 'D'
            txt(i) = 'W';
        case 'E'
            txt(i) = 'V';
        case 'F'
            txt(i) = 'U';
        case 'G'
            txt(i) = 'T';
        case 'H'
            txt(i) = 'S';
        case 'I'
            txt(i) = 'R';
        case 'J'
            txt(i) = 'Q';
        case 'K'
            txt(i) = 'P';
        case 'L'
            txt(i) = 'O';
        case 'M'
            txt(i) = 'N';
        case 'N'
            txt(i) = 'M';
        case 'O'
            txt(i) = 'L';
        case 'P'
            txt(i) = 'K';
        case 'Q'
            txt(i) = 'J';
        case 'R'
            txt(i) = 'I';
        case 'S'
            txt(i) = 'H';
        case 'T'
            txt(i) = 'G';
        case 'U'
            txt(i) = 'F';
        case 'V'
            txt(i) = 'E';
        case 'W'
            txt(i) = 'D';
        case 'X'
            txt(i) = 'C';
        case 'Y'
            txt(i) = 'B';
        case 'Z'
            txt(i) = 'A';
    end
end
coded = txt;
end