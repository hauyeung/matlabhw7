function m = May2015()
m = [];
for i=1:31
d = struct('month', 'May', 'date', i, 'day',datestr(strcat(num2str(i),'-May-2015'),'ddd'));
m = [m d];
end
end